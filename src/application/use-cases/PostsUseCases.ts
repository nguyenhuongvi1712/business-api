import { Post } from 'domain/models/Post';
import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import axios from 'axios';
import { IPostsRepository } from 'application/ports/IPostsRepository';
import { lowerFirst } from 'lodash';
const USER_API_PATH = 'http://localhost:9000/users/';
@Injectable()
export class PostsUseCases {
  private readonly logger = new Logger(PostsUseCases.name);

  constructor(private readonly postsRepository: IPostsRepository) {}

  async getAllPostsByUser(userId: number): Promise<Post[]> {
    this.logger.log('Fetch all user`s posts');
    return await this.postsRepository.find({ userId });
  }

  async getAllPost(): Promise<Post[]> {
    this.logger.log('Fetch all posts');
    return await this.postsRepository.find();
  }

  async getUserByPost(id: number): Promise<any> {
    try {
      const post = await this.postsRepository.findOne({ id });
      if (post) {
        const user = await axios.get(USER_API_PATH + post.userId);
        if (user.data) return user.data;
        else
          throw new NotFoundException(
            `The user {${post.userId}} has not found.`,
          );
      } else {
        throw new NotFoundException(`The post {${id}} has not found.`);
      }
    } catch (error) {
      this.logger.debug(error);
    }
    // const post = await this.postsRepository.findOne({ id });
    // if (!post) throw new NotFoundException(`The post {${id}} has not found.`);
    // const user = await axios.get(USER_API_PATH + post.userId);
    // if (!user)
    //   throw new NotFoundException(`The user {${post.userId}} has not found.`);
    // return user.data;
  }

  async createPost(userId: number, title: string, text: string): Promise<any> {
    try {
      const user = await await axios.get(USER_API_PATH + userId);
      if (!user)
        throw new NotFoundException(`The user {${userId}} wasn't found.`);
      const post = {
        userId,
        title,
        text,
      };
      return await this.postsRepository.save(post);
    } catch (error) {
      this.logger.debug(error);
    }
  }
}
