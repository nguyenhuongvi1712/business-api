import { IEntity } from 'domain/shared/IEntity';

export class Post implements IEntity {
  id?: number;

  title: string;

  text: string;

  userId : number

  createdAt?: Date;

  updatedAt?: Date;

  constructor(title: string, text: string, userId: number, id?: number) {
    this.title = title;
    this.text = text;
    this.userId = userId;
    this.id = id;
  }

  equals(entity: IEntity): boolean {
    if (!(entity instanceof Post)) return false;

    return this.id === entity.id;
  }
}
