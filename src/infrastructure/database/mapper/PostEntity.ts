import { EntitySchema } from 'typeorm';

import { Post } from 'domain/models/Post';

import { BaseEntity } from './BaseEntity';

export const PostEntity = new EntitySchema<Post>({
  name: 'Post',
  tableName: 'posts',
  target: Post,
  columns: {
    ...BaseEntity,
    title: {
      type: String,
      length: 50,
    },
    text: {
      type: String,
    },
    userId: {
      type: Number,
    },
  },
});
