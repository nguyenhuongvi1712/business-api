import { Module } from '@nestjs/common';

import { PostsController } from 'presentation/controllers/PostsController';
import { PostsUseCases } from 'application/use-cases/PostsUseCases';
import { IPostsRepository } from 'application/ports/IPostsRepository';
import { PostsRepository } from 'infrastructure/database/repositories/PostsRepository';
@Module({
  imports: [],
  controllers: [PostsController],
  providers: [
    PostsUseCases,
    { provide: IPostsRepository, useClass: PostsRepository },
  ],
})
export class PostsModule {}
